package com.gruponach.test.dto.response;

import com.gruponach.test.domain.Employees;
import lombok.Data;

import java.util.List;

@Data
public class EmployeeSumWorkedHoursResponse {

   private Long totalWorkedHours;
   private Boolean success;
}
