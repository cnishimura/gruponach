package com.gruponach.test.dto.response;

import com.gruponach.test.domain.Employees;
import lombok.Data;

import java.util.List;

@Data
public class EmployeeResponse {

   private List<Employees> employees;
   private Response message;
}
