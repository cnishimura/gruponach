package com.gruponach.test.dto.response;

import lombok.Data;

@Data
public class Response {

    private Long id;
    private Boolean success;
}
