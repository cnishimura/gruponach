package com.gruponach.test.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmployeeSaveRequest {

    @JsonProperty("gender_id")
    private Long genderId;
    @JsonProperty("job_id")
    private Long jobId;
    private String name;
    private String lastName;
    @JsonProperty("birth_date")
    private String birthdate;
}
