package com.gruponach.test.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmployeeWorkedHoursRequest {

    @JsonProperty("employee_id")
    private Long employeeId;
    @JsonProperty("worked_hours")
    private Long workedHours;
    @JsonProperty("worked_date")
    private String workedDate;
}
