package com.gruponach.test.dto.response;

import lombok.Data;

@Data
public class EmployeeSumSalaryResponse {

   private Long payment;
   private Boolean success;
}
