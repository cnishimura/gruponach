package com.gruponach.test.repository;

import com.gruponach.test.domain.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employees, Long> {

    Employees findByNameAndLastName(String name, String lastName);

    Optional<Employees> findById(Long id);

    @Query(value = "Select emp.id,emp.name,emp.last_name,emp.birthdate, \n" +
            "       job.id,job.name,job.salary,\n" +
            "       emp.id,emp.name\n" +
            "from EMPLOYEES emp\n" +
            "inner join JOBS as job on job.id = emp.job_id\n" +
            "inner join GENDERS as  gen on gen.id = emp.gender_id\n" +
            "where emp.job_id = :JobId", nativeQuery = true)
    List<Employees> getEmployeesByJobId(Long JobId);

    @Query(value = "Select sum(ewh.worked_hours) as total\n" +
            "from EMPLOYEES emp\n" +
            "inner join EMPLOYEE_WORKED_HOURS as ewh on ewh.employee_id = emp.id\n" +
            "where emp.id = :employeeId AND (ewh.worked_date BETWEEN :startDate AND :end_date )\n" +
            "group by ewh.worked_hours", nativeQuery = true)
    Long sum(Long employeeId, LocalDate startDate, LocalDate end_date);

    @Query(value = "Select sum(job.salary) as total\n" +
            "from EMPLOYEES emp\n" +
            "inner join JOBS as job on job.id = emp.job_id\n" +
            "inner join EMPLOYEE_WORKED_HOURS as ewh on ewh.employee_id = emp.id\n" +
            "where emp.id = :employeeId AND (ewh.worked_date BETWEEN :startDate AND :end_date )\n" +
            "group by job.salary", nativeQuery = true)
    Long sumSalary(Long employeeId, LocalDate startDate, LocalDate end_date);
}
