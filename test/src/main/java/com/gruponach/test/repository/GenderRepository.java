package com.gruponach.test.repository;

import com.gruponach.test.domain.Genders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GenderRepository extends JpaRepository<Genders,Long> {

    Optional<Genders> findById(Long id);
}
