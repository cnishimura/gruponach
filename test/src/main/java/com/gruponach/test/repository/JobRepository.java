package com.gruponach.test.repository;

import com.gruponach.test.domain.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JobRepository extends JpaRepository<Jobs,Long> {

    Optional<Jobs> findById(Long id);
}
