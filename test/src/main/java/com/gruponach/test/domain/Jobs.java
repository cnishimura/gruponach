package com.gruponach.test.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "JOBS")
public class Jobs {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobs_generator")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SALARY")
    private Double salary;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="jobs_id", nullable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Employees employees;

}
