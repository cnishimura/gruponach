package com.gruponach.test.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EMPLOYEES_WORKED_HOURS")
public class EmployeeWorkedHours {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employees_worked_hours_generator")
    private Long id;

    @Column(name = "WORKED_HOURS")
    private Long workedHours;

    @Column(name = "WORKED_DATE")
    private LocalDate workedDate;

    @OneToMany(mappedBy="employees_id")
    private Set<Employees> employees;
}
