package com.gruponach.test.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EMPLOYEES")
public class Employees {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employees_generator")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LASTNAME")
    private String lastName;

    @Column(name = "BIRTHDATE")
    private LocalDate birthDate;

    @OneToMany(mappedBy="genders_id")
    private Set<Genders> genders;

    @OneToMany(mappedBy="jobs_id")
    private Set<Jobs> jobs;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="employees_id", nullable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EmployeeWorkedHours employeeWorkedHours;

}
