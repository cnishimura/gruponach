package com.gruponach.test.controller;

import com.gruponach.test.dto.request.EmployeeSaveRequest;
import com.gruponach.test.dto.request.EmployeeSumHoursRequest;
import com.gruponach.test.dto.request.EmployeeSumSalaryRequest;
import com.gruponach.test.dto.request.EmployeeWorkedHoursRequest;
import com.gruponach.test.dto.response.EmployeeResponse;
import com.gruponach.test.dto.response.EmployeeSumSalaryResponse;
import com.gruponach.test.dto.response.EmployeeSumWorkedHoursResponse;
import com.gruponach.test.dto.response.Response;
import com.gruponach.test.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class EmployeeController {

    private final EmployeeService employeeService;


    @PostMapping("/employee")
    public ResponseEntity<Response> createEmployee(@RequestBody EmployeeSaveRequest request) {
        return new ResponseEntity<>(employeeService.createEmployee(request), HttpStatus.CREATED);
    }

    @PostMapping("/employee-worked-hours")
    public ResponseEntity<Response> createEmployeeWorkedHours(@RequestBody EmployeeWorkedHoursRequest request) {
        return new ResponseEntity<>(employeeService.createEmployeeWorkedHours(request), HttpStatus.CREATED);
    }

    @GetMapping("/employee/{jobId}")
    public ResponseEntity<EmployeeResponse> employees(@PathVariable("jobId") Long jobId) {
        return new ResponseEntity<>(employeeService.employees(jobId), HttpStatus.OK);
    }

    @PostMapping("/employee-sum-worked-hours")
    public ResponseEntity<EmployeeSumWorkedHoursResponse> employeeSumWorkedHours(@RequestBody EmployeeSumHoursRequest request) {
        return new ResponseEntity<>(employeeService.employeeSumHoursRequest(request), HttpStatus.OK);
    }

    @PostMapping("/employee-sum-salary")
    public ResponseEntity<EmployeeSumSalaryResponse> employeeSumSalary(@RequestBody EmployeeSumSalaryRequest request) {
        return new ResponseEntity<>(employeeService.employeeSumSalary(request), HttpStatus.OK);
    }




}
