package com.gruponach.test.service;

import com.gruponach.test.domain.EmployeeWorkedHours;
import com.gruponach.test.domain.Employees;
import com.gruponach.test.domain.Genders;
import com.gruponach.test.domain.Jobs;
import com.gruponach.test.dto.request.EmployeeSaveRequest;
import com.gruponach.test.dto.request.EmployeeSumHoursRequest;
import com.gruponach.test.dto.request.EmployeeSumSalaryRequest;
import com.gruponach.test.dto.request.EmployeeWorkedHoursRequest;
import com.gruponach.test.dto.response.EmployeeResponse;
import com.gruponach.test.dto.response.EmployeeSumSalaryResponse;
import com.gruponach.test.dto.response.EmployeeSumWorkedHoursResponse;
import com.gruponach.test.dto.response.Response;
import com.gruponach.test.repository.EmployeeRepository;
import com.gruponach.test.repository.EmployeeWorkedHoursRepository;
import com.gruponach.test.repository.GenderRepository;
import com.gruponach.test.repository.JobRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final GenderRepository genderRepository;
    private final JobRepository jobRepository;

    private final EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    public Response createEmployee(EmployeeSaveRequest request) {
        Response response = new Response();
        response.setSuccess(Boolean.FALSE);

        Employees employeesFindByNameAndLastName =employeeRepository.findByNameAndLastName(request.getName(),request.getLastName());
        if (null != employeesFindByNameAndLastName){
            return  response;
        }

        Genders genders = genderRepository.findById(request.getGenderId()).get();
        if (null == genders){
            return  response;
        }

        Jobs jobs = jobRepository.findById(request.getJobId()).get();
        if (null == jobs){
            return  response;
        }

        Employees employees = new Employees();
        employees.setName(request.getName());
        employees.setLastName(request.getLastName());

        Set<Genders> gendersSet = new HashSet<>();
        gendersSet.add(genders);
        employees.setGenders(gendersSet);

        Set<Jobs> jobsSet  = new HashSet<>();
        jobsSet.add(jobs);
        employees.setJobs(jobsSet);

        employeeRepository.save(employees);
        response.setId(10L);
        response.setSuccess(Boolean.TRUE);
        return response;
    }

    public Response createEmployeeWorkedHours(EmployeeWorkedHoursRequest request) {
        Response response = new Response();
        response.setSuccess(Boolean.FALSE);
        Employees employeesFindById =employeeRepository.findById(request.getEmployeeId()).get();

        if (null == employeesFindById){
            return  response;
        }

        EmployeeWorkedHours employeeWorkedHours = new EmployeeWorkedHours();
        employeeWorkedHours.setWorkedHours(request.getWorkedHours());
        Set<Employees> employeesSet = new HashSet<>();
        employeesSet.add(employeesFindById);
        employeeWorkedHours.setEmployees(employeesSet);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        employeeWorkedHours.setWorkedDate(LocalDate.parse(request.getWorkedDate(), formatter));
        employeeWorkedHoursRepository.save(employeeWorkedHours);
        response.setId(10L);
        response.setSuccess(Boolean.TRUE);
        return response;
    }

    public EmployeeResponse employees(Long jobId) {
        EmployeeResponse rs = new EmployeeResponse() ;
        Response response = new Response();
        response.setId(10L);
        response.setSuccess(Boolean.TRUE);
        rs.setEmployees(employeeRepository.getEmployeesByJobId(jobId));
        rs.setMessage(response);
        return  rs;
    }

    public EmployeeSumWorkedHoursResponse employeeSumHoursRequest(EmployeeSumHoursRequest request) {
        EmployeeSumWorkedHoursResponse response = new EmployeeSumWorkedHoursResponse();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate startDate = LocalDate.parse(request.getStartDate(), formatter);
        LocalDate endDate = LocalDate.parse(request.getEndDate(), formatter);

        Long sum = employeeRepository.sum(request.getEmployeeId(),startDate,endDate);
        response.setTotalWorkedHours(sum);
        response.setSuccess(Boolean.TRUE);
        return response;
    }

    public EmployeeSumSalaryResponse employeeSumSalary(EmployeeSumSalaryRequest request) {
        EmployeeSumSalaryResponse response = new EmployeeSumSalaryResponse();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate startDate = LocalDate.parse(request.getStartDate(), formatter);
        LocalDate endDate = LocalDate.parse(request.getEndDate(), formatter);
        Long sum = employeeRepository.sumSalary(request.getEmployeeId(),startDate,endDate);
        response.setPayment(sum);
        response.setSuccess(Boolean.TRUE);

        return response;

    }
}
