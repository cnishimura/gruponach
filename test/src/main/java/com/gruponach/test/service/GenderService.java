package com.gruponach.test.service;

import com.gruponach.test.repository.GenderRepository;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class GenderService {

    private final GenderRepository genderRepository;

}
